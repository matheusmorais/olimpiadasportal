package br.sp.senac.bsi5.olimpiadasportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter {
	// a partir da verifica��o de usuario no controller o getSession retorna uma
	// string que � verificada no metodo abaixo, se for verdadeiro ele retorna
	// que o usuario � verdadeiro dentro da sessao
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller)
			throws Exception {
		String uri = request.getRequestURI();
		if (uri.endsWith("/") || uri.endsWith("verificaUsuario") || uri.endsWith("cadastro")
				|| uri.endsWith("adicionaUsuario") || uri.contains("resources"))  {
			return true;
		}
		if (request.getSession().getAttribute("usuarioLogado") != null) {
			return true;
		}
		response.sendRedirect(" ");
		return false;
	}
}