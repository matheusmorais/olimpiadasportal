package br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.sp.senac.bsi5.olimpiadasportal.model.Usuario;

public class CRUD_Usuario {

	private Connection con;

	public CRUD_Usuario() {

		this.con = Banco.abrirConexao();
	}

	public void salvar(Usuario usuario) throws SQLException {
		String sql = "insert into user(nome, email, pw) values (?, ?,?)";
		PreparedStatement pst = con.prepareStatement(sql);
		pst.setString(1, usuario.getNome());
		pst.setString(2, usuario.getEmail());
		pst.setString(3, usuario.getSenha());
		pst.execute();
	}
	/*
	 * public Usuario findById(long id) throws SQLException { String sql =
	 * "select * from usuario where id = ?"; PreparedStatement pst =
	 * con.prepareStatement(sql); pst.setLong(1, id); ResultSet rs =
	 * pst.executeQuery();
	 * 
	 * if (rs.next()) { Usuario usuario = new Usuario(null, null, null);
	 * usuario.setNome(rs.getString("nome"));
	 * usuario.setPw(rs.getString("senha")); usuario.setId(rs.getLong("id"));
	 * return usuario; } else { return null; } }
	 */

	public boolean autentica(Usuario usuario) throws SQLException {
		String sql = "select * from user where email = ? and pw = ?";
		PreparedStatement pst = con.prepareStatement(sql);
		pst.setString(1, usuario.getEmail());
		pst.setString(2, usuario.getSenha());
		ResultSet rs = pst.executeQuery();
		return rs.next();
	}

}
