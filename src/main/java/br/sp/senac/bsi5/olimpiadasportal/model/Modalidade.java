package br.sp.senac.bsi5.olimpiadasportal.model;

import java.util.ArrayList;

public  class Modalidade {
	
	private String id_nome;
	private String nome;
	private String introducao;
	private ArrayList<Investimento> investimentos;
	
	public Modalidade(String nome){
		id_nome = null;
		this.nome = nome;
		introducao = "";
		investimentos = new ArrayList<Investimento>();
	}
	
	public Modalidade(String id_nome, String nome, String introducao){
		this.id_nome = id_nome;
		this.nome = nome;
		this.introducao = introducao;
	}
	
	public void adicionarInvestimento(Investimento i){
		investimentos.add(i);
	}
	
	public void imprimirInvestimentos(){
		for(int i = 0; i < investimentos.size(); i++){
			investimentos.get(i).imprimirInvestimento();
		}
	}
	
	
	public void imprimirInfo(){
		System.out.println(nome + "\n" + introducao + "\n" + "\n" + "Investimentos:\n");
		
		imprimirInvestimentos();
		
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getIntroducao() {
		return introducao;
	}

	
	public void setIntroducao(String introducao) {
		this.introducao = introducao;
	}

	public ArrayList<Investimento> getInvestimentos() {
		return investimentos;
	}

	public void setInvestimentos(ArrayList<Investimento> investimentos) {
		this.investimentos = investimentos;
	}

	public String getId_nome() {
		return id_nome;
	}

	public void setId_nome(String id_nome) {
		this.id_nome = id_nome;
	}
}