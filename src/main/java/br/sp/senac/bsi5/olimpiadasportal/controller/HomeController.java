package br.sp.senac.bsi5.olimpiadasportal.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql.CRUD_Usuario;
import br.sp.senac.bsi5.olimpiadasportal.model.Usuario;

@Controller
public class HomeController {

	// transacao das paginas
	@RequestMapping(value = {" ", "voltar"})
	public ModelAndView test(HttpServletRequest request, HttpServletResponse response) throws IOException {

		return new ModelAndView("home");
	}

	// transacao das paginas
	@RequestMapping(value = { "sucess/", "modalidades" })
	public ModelAndView modalidades(HttpServletResponse response) throws IOException {
		return new ModelAndView("modalidades");
	}
	
	// transacao das paginas
	@RequestMapping(value = "/regionais")
	public ModelAndView regional(HttpServletResponse response) throws IOException {
		return new ModelAndView("regional");
	}
	
	// transacao das paginas
	@RequestMapping(value = "/histogramatotal")
	public ModelAndView histogramatotal(HttpServletResponse response) throws IOException {
		return new ModelAndView("histogramatotal");
	}
	
	// transacao das paginas
	@RequestMapping(value = "/histogramatop10")
	public ModelAndView histogramatop10(HttpServletResponse response) throws IOException {
		return new ModelAndView("histogramatop10");
	}
	
	// transacao das paginas
	@RequestMapping(value = "/histogramaanual")
	public ModelAndView histogramaanual(HttpServletResponse response) throws IOException {
		return new ModelAndView("histogramaanual");
	}

	// transacao das paginas
	@RequestMapping(value = "/graficos")
	public ModelAndView graficos(HttpServletResponse response) throws IOException {
		return new ModelAndView("grafico");
	}

	// transacao das paginas
	@RequestMapping(value = "/cadastro")
	public ModelAndView cadastro(HttpServletResponse response) throws IOException {
		return new ModelAndView("cadastro");
	}
	
	// adicionando usuario
	@RequestMapping(value = "/adicionaUsuario", method = RequestMethod.POST)
	public String doPostAdiciona(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Usuario usuario = new Usuario(null, null, null);
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String senha = req.getParameter("senha");

		usuario.setNome(nome);
		usuario.setEmail(email);
		usuario.setSenha(senha);

		CRUD_Usuario dao = new CRUD_Usuario();

		try {
			dao.salvar(usuario);
			req.setAttribute("nome", usuario);
			req.setAttribute("email", usuario);
			req.setAttribute("senha", usuario);
			// getServletContext().getRequestDispatcher("/sucesso.jsp").forward(req,
			// resp);
			return ("/sucess");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	// verifica usuario
	@RequestMapping(value = "/verificaUsuario", method = RequestMethod.POST)
	public String doPostVerifica(HttpServletRequest req, HttpServletResponse resp, HttpSession session)
			throws ServletException, IOException, SQLException {

		CRUD_Usuario dao = new CRUD_Usuario();
		Usuario usuario = new Usuario(null, null, null);
		String email = req.getParameter("email");
		String senha = req.getParameter("senha");
		usuario.setEmail(email);
		usuario.setSenha(senha);

		if (dao.autentica(usuario) == true) {
			session.setAttribute("usuarioLogado", usuario);
			System.out.println("ok, eu entrei nessa bucetinha gostosa");
			return "modalidades";
		} else {
			System.out.println("porra, nao entrei nessa bucetinha");
			return "falhaLogin";
		}
	}

	// logout do usuario
	@RequestMapping("/logout")
	public String sair(HttpSession session) {
		session.invalidate();
		return "redirect:/";

	}

}
