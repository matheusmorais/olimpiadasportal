package br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.math.BigDecimal;


import com.mysql.jdbc.PreparedStatement;



import br.sp.senac.bsi5.olimpiadasportal.model.Investimento;
import br.sp.senac.bsi5.olimpiadasportal.model.Modalidade;

public class CRUD_Investimento extends CRUD{

	public CRUD_Investimento(){
		super();
	}
	
	public static void inserirInvestimento(Investimento i, Modalidade m) throws SQLException{
		String s1 = CRUD_Estado.buscarChavePrimaria(i.getUf());
		int id_estado = Integer.parseInt(s1);
		
		String s2 = CRUD_Modalidade.buscarChavePrimaria(m.getNome());
		int id_modalidade = Integer.parseInt(s2);
		
		Banco.abrirConexao();
		
		query = "insert into investimentos(id_estado, id_modalidade, numeroConvenio, objetivoConvenio, orgaoSuperior,"
				+"concedente, convenente, publicacao, inicioVigencia, fimVigencia,"
				+"dataUltimaLiberacao, valorConvenio, valorLiberado, valorContraPartida, valorUltimaLiberacao)"
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		
		
		ps.setInt(1, id_estado);
		ps.setInt(2, id_modalidade);
		
		int numeroConvenio = Integer.parseInt(i.getNumeroConvenio());
		ps.setInt(3, numeroConvenio);
		
		ps.setString(4, i.getObjetivoConvenio());
		ps.setString(5, i.getOrgaoSuperior());
		ps.setString(6, i.getConcedente());
		ps.setString(7, i.getConvenente());
		
		ps.setString(8, i.getPublicacao());
		ps.setString(9, i.getInicioVigencia());
		ps.setString(10, i.getFimVigencia());
		ps.setString(11, i.getDataUltimaLiberacao());
		
		BigDecimal valorConvenio = new BigDecimal(i.getValorConvenio());
		ps.setBigDecimal(12, valorConvenio);
		
		BigDecimal valorLiberado = new BigDecimal(i.getValorLiberado());
		ps.setBigDecimal(13, valorLiberado);
		
		BigDecimal valorContraPartida = new BigDecimal(i.getValorContraPartida());
		ps.setBigDecimal(14, valorContraPartida);
		
		BigDecimal valorUltimaLiberacao = new BigDecimal(i.getValorUltimaLiberacao());
		ps.setBigDecimal(15, valorUltimaLiberacao);
		
		if(ps.executeUpdate() > 0){
			System.out.println("Investimento: " + i.getNumeroConvenio() +", inserido com sucesso.");
		}
		
		Banco.fecharConexao();
	}
	
	

	/*retorna a lista de modalidades para a tabela de investimentos e contratos.*/
	
	public static ArrayList<String> buscarModalidadesTabelaEstadosModalidades() throws SQLException{
		Banco.abrirConexao();
		query = "select m.nome, e.nome from modalidades m join investimentos i on i.id_modalidade = m.id_modalidade "
				+ "join estados e on  e.id_estado = i.id_estado group by i.id_estado, i.id_modalidade";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		ArrayList<String> lista = null;
		
		if(resultado != null){
			lista = new ArrayList<String>();
			while(resultado.next()){
				lista.add(resultado.getString(1));
			}
		}	
		
		
		Banco.fecharConexao();
		return lista;
	}
	
	
	
	/*retorna a lista de estados para a tabela de investimentos e contratos.*/
	
	public static ArrayList<String> buscarEstadosTabelaEstadosModalidades() throws SQLException{
		Banco.abrirConexao();
		query = "select m.nome, e.nome from modalidades m join investimentos i on i.id_modalidade = m.id_modalidade "
				+ "join estados e on  e.id_estado = i.id_estado group by i.id_estado, i.id_modalidade";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		ArrayList<String> lista = null;
		
		if(resultado != null){
			lista = new ArrayList<String>();
			while(resultado.next()){
				lista.add(resultado.getString(2));
			}
		}	
		
		Banco.fecharConexao();
		return lista;
	}
	
	
	
	
	
	/*Método retorna uma lista com todas as modalidades do id, passado no parâmetro*/
	
	public static ArrayList<Investimento> buscarInvestimentos(String id) throws SQLException{
		Banco.abrirConexao();
		query = "select * from investimentos where id_modalidade =  " + id;
		ArrayList<Investimento> lista = null;
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
			
		if(resultado != null){
			lista = new ArrayList<Investimento>();
			while(resultado.next()){
				String id_investimento = resultado.getString(1);
				String id_estado = resultado.getString(2);
				String id_modalidade = resultado.getString(3);
				String numeroConvenio = resultado.getString(4);
				String objetivoConvenio = resultado.getString(5);
				String orgaoSuperior = resultado.getString(6);
				String concedente = resultado.getString(7);
				String convenente = resultado.getString(8);
				String publicacao = resultado.getString(9);
				String inicioVigencia = resultado.getString(10);
				String fimVigencia = resultado.getString(11);

				
				

				String dataUltimaLiberacao = resultado.getString(12);
				String valorConvenio = resultado.getString(13);
				String valorLiberado = resultado.getString(14);
				String valorContraPartida = resultado.getString(15);
				String valorUltimaLiberacao = resultado.getString(16);


				/*Instanciar Investimento*/
				Investimento i = new Investimento(id_investimento, id_estado, id_modalidade, numeroConvenio, objetivoConvenio,
						orgaoSuperior, concedente, convenente, publicacao, inicioVigencia, fimVigencia,
						dataUltimaLiberacao, valorConvenio, valorLiberado, valorContraPartida, valorUltimaLiberacao);
				lista.add(i);
				i = null;
			}
		}
		
		Banco.fecharConexao();
		
		return lista;
	}
	
	
	public static ArrayList<BigDecimal> buscarValorConvenioTotalIdModalidade() throws SQLException{
		Banco.abrirConexao();
		ArrayList<BigDecimal> lista = null;
		
		query = "select sum(valorConvenio) from investimentos group by id_modalidade";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		
		if(resultado != null){
			lista = new ArrayList<BigDecimal>();
			while(resultado.next()){
				BigDecimal valor = new BigDecimal(resultado.getString(1));
				lista.add(valor);
			}
		}	
		
		Banco.fecharConexao();
		return lista;
	}
	
	public static ArrayList<BigDecimal> buscarValorConvenioTotalIdModalidadeTop10() throws SQLException{
		Banco.abrirConexao();
		ArrayList<BigDecimal> lista = null;
		
		query = "select sum(valorLiberado) from investimentos group by id_modalidade order by sum(valorLiberado) desc limit 10";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		
		if(resultado != null){
			lista = new ArrayList<BigDecimal>();
			while(resultado.next()){
				BigDecimal valor = new BigDecimal(resultado.getString(1));
				lista.add(valor);
			}
		}	
		
		Banco.fecharConexao();
		return lista;
	}

	public static ArrayList<BigDecimal> buscarValorConvenioTotalIdEstado() throws SQLException{
		Banco.abrirConexao();
		ArrayList<BigDecimal> lista = null;
		
		query = "select sum(valorConvenio) from investimentos group by id_estado";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		
		if(resultado != null){
			lista = new ArrayList<BigDecimal>();
			while(resultado.next()){
				BigDecimal valor = new BigDecimal(resultado.getString(1));
				lista.add(valor);
			}
		}	
		
		Banco.fecharConexao();
		return lista;
	}

	
	/************************** Método retorna valor total investido, ao ano, passado no parâmetro. 
	 * @throws SQLException *******************************************/
	
	public static BigDecimal buscarValorTotalAnual(int ano) throws SQLException{
		Banco.abrirConexao();
		BigDecimal total = null;
		
		query = "SELECT sum(valorUltimaLiberacao) FROM investimentos WHERE dataUltimaLiberacao BETWEEN " + "'" + ano + "-01-01' AND '" + ano
				+ "-12-31'";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		
	
		if(resultado != null){
			while(resultado.next()){
				total = new BigDecimal(resultado.getString(1));
			}
		}
		
		Banco.fecharConexao();
		return total;

	}
	
	
}	
	

