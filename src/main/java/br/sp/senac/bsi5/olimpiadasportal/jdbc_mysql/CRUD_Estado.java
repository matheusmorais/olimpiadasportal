package br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

public class CRUD_Estado extends CRUD{

	public CRUD_Estado(){
		super();
	}
	
	/*Método retorna a pk do valor passado no parâmetro*/
	public static String buscarChavePrimaria(String valor) throws SQLException{
		Banco.abrirConexao();
		
		String pk = null;
		query = "select id_estado from estados where sigla = " + "'" + valor + "'  || nome = " + "'" + valor + "'";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		
		if(resultado != null){
			while(resultado.next()){
				pk  = resultado.getString(1);
			}
		}
		
		Banco.fecharConexao();
		return pk;
	}
	
	
	/*Métodos retorna uma lista com os nomes de todos os estados*/
	
	public static ArrayList<String> buscarNomes() throws SQLException{
		ArrayList<String> lista = null;
		Banco.abrirConexao();
		
		query = "select nome from estados";
		ps = (PreparedStatement) Banco.getConexao().prepareStatement(query);
		ResultSet resultado = ps.executeQuery();
		
		if(resultado != null){
			lista = new ArrayList<String>();
			
			while(resultado.next()){
				String nome = resultado.getString(1);
				lista.add(nome);
				nome = null;
			}
		}
		
		Banco.fecharConexao();
		
		return lista;
	}





}



