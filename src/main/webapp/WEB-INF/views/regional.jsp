
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql.CRUD_Modalidade"%>
<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql.CRUD_Investimento"%>
<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql.CRUD_Estado"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.math.BigDecimal"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%
	ArrayList<String> lm = CRUD_Modalidade.buscarNomes();
	ArrayList<BigDecimal> litm = CRUD_Investimento.buscarValorConvenioTotalIdModalidade();
	ArrayList<BigDecimal> lite = CRUD_Investimento.buscarValorConvenioTotalIdEstado();
%>



<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<!--Gráfico de Colunas -->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    
   	  google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawStuff);
		
      function drawStuff() {
    	  var data = google.visualization.arrayToDataTable([
    	        ['', 'Modalidades', { role: 'style' }],
    	                                      	
    	                                          	      
    	        //nome,valor 
    	     
    	        ['<%=lm.get(0)%>', <%=litm.get(0)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(1)%>', <%=litm.get(1)%>, 'gold'],
    	        ['<%=lm.get(2)%>', <%=litm.get(2)%>, 'color: #e5e4e2' ], // CSS-style declaration
    	        ['<%=lm.get(3)%>', <%=litm.get(3)%>, '#b87333'], // RGB value
    	                                          	      
    	        ['<%=lm.get(4)%>', <%=litm.get(4)%>, 'gold'],
    	      	['<%=lm.get(5)%>', <%=litm.get(5)%>, 'color: #e5e4e2' ],                                          
    	        ['<%=lm.get(6)%>', <%=litm.get(6)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(7)%>', <%=litm.get(7)%>, 'gold'],
    	                                          	      
    	        ['<%=lm.get(8)%>', <%=litm.get(8)%>, 'gold'],
    	        ['<%=lm.get(9)%>', <%=litm.get(9)%>, 'color: #e5e4e2' ],                                          
    	                                      			  

    	        ['<%=lm.get(10)%>', <%=litm.get(10)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(11)%>', <%=litm.get(11)%>, 'gold'],
    	                                          	      
    	        ['<%=lm.get(12)%>', <%=litm.get(12)%>, 'gold'],
    	        ['<%=lm.get(13)%>', <%=litm.get(13)%>, 'color: #e5e4e2' ],                                          
    	        ['<%=lm.get(14)%>', <%=litm.get(14)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(15)%>', <%=litm.get(15)%>, 'gold'],
    	                                          	      
    	        ['<%=lm.get(16)%>', <%=litm.get(16)%>, 'gold'],
    	        ['<%=lm.get(17)%>', <%=litm.get(17)%>, 'color: #e5e4e2' ],                                          
    	                                      			  
    	        ['<%=lm.get(18)%>', <%=litm.get(18)%>, '#b87333'], // RGB value
    	                                          	      
    	        ['<%=lm.get(19)%>', <%=litm.get(19)%>, 'gold'],
    	                                          	      
    	                                          	      
    	        ['<%=lm.get(20)%>', <%=litm.get(20)%>, 'gold'],
    	        ['<%=lm.get(21)%>', <%=litm.get(21)%>, 'color: #e5e4e2' ],                                          
    	        ['<%=lm.get(22)%>', <%=litm.get(22)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(23)%>', <%=litm.get(23)%>, 'gold'],
    	                                          	      
    	       	['<%=lm.get(24)%>', <%=litm.get(24)%>, 'gold'],
   	        	['<%=lm.get(25)%>', <%=litm.get(25)%>, 'color: #e5e4e2' ],                                          
	
    	 ]);	
    	  
        var options = {
          width: 1500,
          chart: {
            title: 'Gráfico de Modalidades',
            subtitle: 'Valor Total de Investimentos em cada Modalidade'
          },
          series: {
            0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              distance: {label: ''}, // Left y-axis.
              brightness: {side: 'right', label: 'apparent magnitude'} // Right y-axis.
            }
          }
        };

      var chart = new google.charts.Bar(document.getElementById('Grafico_Coluna'));
      chart.draw(data, options);
    };
    </script>

<!--Gráfico de Colunas -->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    
   	  google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawStuff);
		
      function drawStuff() {
    	  var data = google.visualization.arrayToDataTable([
    	        ['', 'Modalidades', { role: 'style' }],
    	                                      	
    	                                          	      
    	        //nome,valor 
    	     
    	        ['<%=lm.get(24)%>', <%=litm.get(0)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(0)%>', <%=litm.get(1)%>, 'gold'],
    	        ['<%=lm.get(2)%>', <%=litm.get(2)%>, 'color: #e5e4e2' ], // CSS-style declaration
    	        ['<%=lm.get(12)%>', <%=litm.get(3)%>, '#b87333'], // RGB value
    	                                          	      
    	        ['<%=lm.get(15)%>', <%=litm.get(4)%>, 'gold'],
    	      	['<%=lm.get(9)%>', <%=litm.get(5)%>, 'color: #e5e4e2' ],                                          
    	        ['<%=lm.get(21)%>', <%=litm.get(6)%>, '#b87333'], // RGB value
    	        ['<%=lm.get(7)%>', <%=litm.get(7)%>, 'gold'],
    	                                          	      
    	        ['<%=lm.get(10)%>', <%=litm.get(8)%>, 'gold'],
    	        ['<%=lm.get(13)%>', <%=litm.get(9)%>, 'color: #e5e4e2' ],                                          
    	                                      			  
				]);	
    	  
        var options = {
          width: 1500,
          chart: {
            title: 'Investimentos modalidades top 10',
            subtitle: 'Valor Total de Investimentos em cada Modalidade'
          },
          series: {
            0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              distance: {label: ''}, // Left y-axis.
              brightness: {side: 'right', label: 'apparent magnitude'} // Right y-axis.
            }
          }
        };

      var chart = new google.charts.Bar(document.getElementById('Grafico_Coluna'));
      chart.draw(data, options);
    };
    </script>


<!--Gráfico Pizza - Estado  -->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Distrito Federal',<%=lite.get(0)%>],
          ['Paraná', <%=lite.get(1)%>],
          ['Rio De Janeiro', <%=lite.get(2)%>],
          ['São Paulo', <%=lite.get(3)%>],
          ['Sergipe', <%=lite.get(4)%>]
	    ]);

        var options = {
          title: 'Investimentos nos Estados',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('Grafico_Estados'));
        chart.draw(data, options);
      }
    </script>

<!-- Gráfico estados do Brasil -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["geochart"]});
      google.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {

        var data = google.visualization.arrayToDataTable([
          ['Estados', 'Investimento'],
          ['Distrito Federal',<%=lite.get(0)%>],
          ['Paraná', <%=lite.get(1)%>],
          ['Rio De Janeiro', <%=lite.get(2)%>],
          ['São Paulo', <%=lite.get(3)%>],
          ['Sergipe', <%=lite.get(4)%>]
          
        ]);

        var options = {
        		title: 'Investimentos nos Estados',
        		is3D: true,
        };
        
        options = {};
        options['region'] = 'BR';
        options['resolution'] = 'provinces';
        
        

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }
    </script>


	<!-- Gráfico Colunas Anual -->
	<%
		BigDecimal t1 = CRUD_Investimento.buscarValorTotalAnual(2011);
		BigDecimal t2 = CRUD_Investimento.buscarValorTotalAnual(2012);
		BigDecimal t3 = CRUD_Investimento.buscarValorTotalAnual(2013);
		BigDecimal t4 = CRUD_Investimento.buscarValorTotalAnual(2014);
		BigDecimal t5 = CRUD_Investimento.buscarValorTotalAnual(2015);
	%>
	
	
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Move', 'Percentage'],
          
          ["2011", <%=t1%>],
		  ["2012", <%=t2%>],
          ["2013", <%=t3%>],
          ["2014", <%=t4%>],
          ["2015", <%=t5%>],
		]);

        var options = {
          title: 'Chess opening moves',
          width: 900,
          legend: { position: 'none' },
          chart: { subtitle: 'popularity by percentage' },
          axes: {
            x: {
              0: { side: 'top', label: 'White to move'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('Grafico_Anual'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
	


	<!--Tabela -->
	
	<%
		ArrayList<String> listaModalidades =  CRUD_Investimento.buscarModalidadesTabelaEstadosModalidades();
		ArrayList<String> listaEstados =  CRUD_Investimento.buscarEstadosTabelaEstadosModalidades();
	%>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1.1", {packages:["table"]});
      google.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Modalidade');
        data.addColumn('string', 'Estado');
        
        data.addRows([
            ['<%=listaModalidades.get(0)%>', '<%=listaEstados.get(0)%>'],
            ['<%=listaModalidades.get(1)%>', '<%=listaEstados.get(1)%>'],
            ['<%=listaModalidades.get(2)%>', '<%=listaEstados.get(2)%>'],
            ['<%=listaModalidades.get(3)%>', '<%=listaEstados.get(3)%>'],
            ['<%=listaModalidades.get(4)%>', '<%=listaEstados.get(4)%>'],
            ['<%=listaModalidades.get(5)%>', '<%=listaEstados.get(5)%>'],
            ['<%=listaModalidades.get(6)%>', '<%=listaEstados.get(6)%>'],
            ['<%=listaModalidades.get(7)%>', '<%=listaEstados.get(7)%>'],
            ['<%=listaModalidades.get(8)%>', '<%=listaEstados.get(8)%>'],
            ['<%=listaModalidades.get(9)%>', '<%=listaEstados.get(9)%>'],
            ['<%=listaModalidades.get(10)%>', '<%=listaEstados.get(10)%>'],
            ['<%=listaModalidades.get(11)%>','<%=listaEstados.get(11)%>'],
            ['<%=listaModalidades.get(12)%>', '<%=listaEstados.get(13)%>'],
            ['<%=listaModalidades.get(13)%>', '<%=listaEstados.get(13)%>'],
            ['<%=listaModalidades.get(14)%>', '<%=listaEstados.get(14)%>'],
            ['<%=listaModalidades.get(15)%>', '<%=listaEstados.get(15)%>'],
            ['<%=listaModalidades.get(16)%>', '<%=listaEstados.get(16)%>'],
            ['<%=listaModalidades.get(17)%>', '<%=listaEstados.get(17)%>'],
            ['<%=listaModalidades.get(18)%>', '<%=listaEstados.get(18)%>'],
            ['<%=listaModalidades.get(19)%>', '<%=listaEstados.get(19)%>'],
            ['<%=listaModalidades.get(20)%>', '<%=listaEstados.get(20)%>'],
            ['<%=listaModalidades.get(21)%>', '<%=listaEstados.get(21)%>'],
            ['<%=listaModalidades.get(22)%>', '<%=listaEstados.get(22)%>'],
            ['<%=listaModalidades.get(23)%>', '<%=listaEstados.get(23)%>'],
            ['<%=listaModalidades.get(24)%>', '<%=listaEstados.get(24)%>'],
            ['<%=listaModalidades.get(25)%>', '<%=listaEstados.get(25)%>'],
            ['<%=listaModalidades.get(26)%>', '<%=listaEstados.get(26)%>'],
            ['<%=listaModalidades.get(27)%>', '<%=listaEstados.get(27)%>'],
            ['<%=listaModalidades.get(28)%>', '<%=listaEstados.get(28)%>'],
            ['<%=listaModalidades.get(29)%>', '<%=listaEstados.get(29)%>']
              
        
            
          ]);

        var table = new google.visualization.Table(document.getElementById('tabela'));

        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
    </script>
	





</head>
<body>



	<nav class="navbar navbar-fixed-top navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"></a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">

				<li><a href="/">Inicio</a></li>

				<li><a href="modalidades">Investimentos</a></li>

				

			</ul>
			<!-- dropdown -->
			<div id="navbar" class="dropdown">

				<button class="btn btn-link" type="button" data-toggle="dropdown">
					Gráficos<span class="caret"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-left">
					<li><a href="regionais">Regional</a></li>

					<li><a href="histogramatop10">Histograma Top 10</a></li>
					<li><a href="histogramatotal">Histograma Total</a></li>
					<li><a href="histogramaanual">Histograma Anual</a></li>
				</ul>
			</div>
			<!-- /dropdown -->
			<ul class="nav navbar-nav navbar-right">
				<li><a href="logout">Sair</a></li>
			</ul>
		</div>
		<!-- /.nav-collapse -->
	</div>
	<!-- /.container --> </nav>
	<!-- /.navbar -->

	<div class="container">
		<br> <br> <br> <br>
		<div class="blog-header">
			<h1 class="blog-title">Gráficos Investimentos</h1>
		</div>

		<div class="row">

			<div class="col-sm-8 blog-main">

				<div class="blog-post">

					<!--Dimensionamento Gráficos-->
					<div id="Grafico_Estados" style="width: 900px; height: 500px;"></div>
					<div id="regions_div" style="width: 900px; height: 500px;"></div> -->
					<div id="tabela"></div>
					
				</div>

			</div>

			<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
				

				<!-- boloco links externos -->
				<div class="sidebar-module">
					<h4>Outros</h4>
					<ol class="list-unstyled">
						<li><a
							href="http://www.portaldatransparencia.gov.br/rio2016/">Portal
								da Transparencia</a></li>
					</ol>
				</div>
				<!-- /boloco links externos -->
			</div>

		</div>

	</div>

	<footer class="blog-footer">
	<p>
		Olímpiadas Rio 2016 <a href="http://getbootstrap.com">Bootstrap</a> by
		NITEIP.
	</p>
	<p>
		<a href="#">Back to top</a>
	</p>
	</footer>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script
		src="C:\Users\Matheus\Dropbox\5° Semestre\PIV - Dev Project\dist/js/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="C:\Users\Matheus\Dropbox\5° Semestre\PIV - Dev Project\dist/assets/js/ie10-viewport-bug-workaround.js"></script>

	<script src="offcanvas.js"></script>
</body>
</html>
