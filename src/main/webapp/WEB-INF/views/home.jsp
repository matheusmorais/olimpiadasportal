<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.model.WhatsNews"%>
<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.service.Extrair_WhatsNews"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>NITEIP Olimpíadas Rio 2016</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="signin.css" rel="stylesheet">
<script src="<c:url value="/resources/js/my-script.js"/>"></script>


</head>

<body>
	<nav class="navbar navbar-fixed-top navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/">Inicio</a></li>
				</ul>
			</div>
			<!-- /.nav-collapse -->
		</div>
		<!-- /.container -->
	</nav>
	<!-- /.navbar -->

	<div class="container">
		<br> <br> <br> <br>
		<div class="blog-header">
			<h1 class="blog-title">Tudo sobre as Olimpíadas Rio 2016</h1>
		</div>
		
		
		<div class="row">

			<div class="col-sm-8 blog-main">

				<div class="blog-post">
					<h2 class="blog-post-title">${modalidade.getNome()}</h2>
					<p class="blog-post-meta">
						
					</p>

					<hr>
					<%
						Extrair_WhatsNews ew = new Extrair_WhatsNews();
						WhatsNews wa = ew.extrairDados();
					%>
					<%=wa.getConteudo() %>
				
				</div>

				<nav>
					<ul class="pager">

						<li><a href="#">Anterior</a></li>
						<li><a href="#">Próximo</a></li>
					</ul>
				</nav>

			</div>

			<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
				
				
				<div class="sidebar-module sidebar-module-inset">
					<h4>Entre e veja mais!</h4>
					<p>Para saber mais sobre as olimpiadas Rio 2016, entre ou
						cadastre-se.</p>
					<p>Após isso você terá visibilidade dos valores investidos nas
						olímpiadas</p>
				</div>
				<!-- coluna modalidades -->
				<div class="sidebar-module">

					<form class="form-signin" method="post" action="verificaUsuario">

						<label class="sr-only">Endereço de E-mail</label> <input
							style="width: 250px;" name="email" type="email"
							class="form-control" placeholder="E-mail" required autofocus>
						<label class="sr-only">Senha</label> <input style="width: 250px;"
							name="senha" type="password" class="form-control"
							placeholder="Senha" required>
						<div class="checkbox">
							<label> <input type="checkbox" value="remember-me">
								Lembre-me
							</label>
						</div>
						<button style="width: 250px;" class="btn btn-info" type="submit">Entrar</button>
					</form>
					<form class="form-signin" method="post" action="cadastro">
						<button style="width: 250px;" class="btn btn-info btn-sm"
							type="submit">Cadastre-se</button>
					</form>
				</div>
				<!-- /coluna modalidades -->
				<!-- boloco links externos -->
				<div class="sidebar-module">
					<h4>Outros</h4>
					<ol class="list-unstyled">
						<li><a
							href="http://www.portaldatransparencia.gov.br/rio2016/">Portal
								da Transparencia</a></li>
					</ol>
				</div>
				<!-- /boloco links externos -->
			</div>

		</div>

	</div>

	<footer class="blog-footer">
		<p>
			Olímpiadas Rio 2016 <a href="http://getbootstrap.com">Bootstrap</a>
			by NITEIP.
		</p>
		<p>
			<a href="#">Back to top</a>
		</p>
	</footer>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script
		src="C:\Users\Matheus\Dropbox\5° Semestre\PIV - Dev Project\dist/js/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="C:\Users\Matheus\Dropbox\5° Semestre\PIV - Dev Project\dist/assets/js/ie10-viewport-bug-workaround.js"></script>

	<script src="offcanvas.js"></script>
</body>
</html>
