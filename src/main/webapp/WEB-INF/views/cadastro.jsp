<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">


<title>Signin Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="signin.css" rel="stylesheet">

<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/js/npm.js"/>"></script>


</head>

<body>

	<div class="container">

		<form class="form-signin" method="post" action="adicionaUsuario">
			<h2 class="form-signin-heading">Cadastro</h2>
			<label class="sr-only">Nome</label> <input class="form-control"
				placeholder="Nome" name="nome"> <label for="inputEmail"
				class="sr-only">Endereço de E-mail</label> <input type="email"
				id="inputEmail" class="form-control"
				placeholder="Endereço de E-mail" name="email" required autofocus>
			<label for="inputPassword" class="sr-only">Senha</label> <input
				type="password" id="inputPassword" name="senha" class="form-control"
				placeholder="Senha" required>
			<div class="checkbox"></div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Confirmar</button>
		</form>

	</div>
	<!-- /container -->
</body>
</html>