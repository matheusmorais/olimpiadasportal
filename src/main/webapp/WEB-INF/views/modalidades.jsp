<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql.CRUD_Modalidade"%>
<%@ page
	import="br.sp.senac.bsi5.olimpiadasportal.jdbc_mysql.CRUD_Investimento"%>
<%@ page import="br.sp.senac.bsi5.olimpiadasportal.model.Modalidade"%>
<%@ page import="br.sp.senac.bsi5.olimpiadasportal.model.Investimento"%>
<%@ page import=" java.util.ArrayList"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Off Canvas Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>

<body>
	<!-- SCRIPLET PARA REALIZAÇÃO DA PAGINA DINAMICA COM AS MODALIDADES -->
	<%
		String valor = request.getParameter("valor");
		Modalidade modalidade = CRUD_Modalidade.buscarModalidade(valor);
		String pk_modalidade = CRUD_Modalidade.buscarChavePrimaria(valor);
		ArrayList<Investimento> investimentos = CRUD_Investimento.buscarInvestimentos(pk_modalidade);
		ArrayList<String> nomes = CRUD_Modalidade.buscarNomes();
	%>

	<nav class="navbar navbar-fixed-top navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">

					<li><a href=" ">Inicio</a></li>

					<li class="active"><a href="modalidades">Investimentos</a></li>

					

				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout">Sair</a></li>
				</ul>
				<!-- dropdown -->
				<div id="navbar" class="dropdown">

					<button class="btn btn-link" type="button" data-toggle="dropdown">
						Gráficos<span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown-menu-left">
						<li><a href="regionais">Regional</a></li>

						<li><a href="histogramatop10">Histograma Top 10</a></li>
						<li><a href="histogramatotal">Histograma Total</a></li>
						<li><a href="histogramaanual">Histograma Anual</a></li>
					</ul>
				</div>
				<!-- /dropdown -->
			</div>



			<!-- /.nav-collapse -->
		</div>
		<!-- /.container -->
	</nav>
	<!-- /.navbar -->

	<div class="container">
		<br> <br> <br> <br>
		<div class="blog-header">
			<h1 class="blog-title">Investimentos</h1>
		</div>

		<div class="row">

			<div class="col-sm-8 blog-main">

				<div class="blog-post">
					<h2 class="blog-post-title"><%=modalidade.getNome()%></h2>
					<p class="blog-post-meta">
						</a>
					</p>

					<hr>
					<p>
						<%=modalidade.getIntroducao()%>
					</p>


					<%
						for (Investimento i : investimentos) {
					%>

					<TABLE class="table table-striped">
						<TR>
							<TD>Número do Convênio:</TD>
							<TD><%=i.getNumeroConvenio()%></TD>
						</TR>
						<TR>
							<TD>Orgão Superior:</TD>
							<TD><%=i.getOrgaoSuperior()%></TD>
						</TR>
						<TR>

							<TD>Concedente:</TD>
							<TD><%=i.getConcedente()%></TD>
						</TR>
						<TR>
							<TD>Convenente:</TD>
							<TD><%=i.getConvenente()%></TD>
						</TR>
						<TR>
							<TD>Objetivo:</TD>
							<TD><%=i.getObjetivoConvenio()%></TD>
						</TR>

						<TR>
							<TD>Publicação:</TD>
							<TD><%=i.getPublicacao()%></TD>
						</TR>

						<TR>
							<TD>Início da Vigência:</TD>
							<TD><%=i.getInicioVigencia()%></TD>
						</TR>

						<TR>
							<TD>Fim da Vigência:</TD>
							<TD><%=i.getFimVigencia()%></TD>
						</TR>
						<TR>
							<TD>Data da última liberação:</TD>
							<TD><%=i.getDataUltimaLiberacao()%></TD>
						</TR>



						<TR>
							<TD>Valor do Convênio:</TD>
							<TD><%=i.getValorConvenio()%></TD>
						</TR>
						<TR>
							<TD>Valor Liberado:</TD>
							<TD><%=i.getValorLiberado()%></TD>
						</TR>
						<TR>
							<TD>Valor Contrapartida:</TD>
							<TD><%=i.getValorContraPartida()%></TD>
						</TR>
						<TR>
							<TD>Valor Ultima Liberação:</TD>
							<TD><%=i.getValorUltimaLiberacao()%></TD>
						</TR>


						<TR>
							<TD></TD>
							<TD></TD>
						</TR>

					</TABLE>

					<br /> <br />
					<%
						}
					%>

				</div>

				

			</div>

			<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
				<div class="sidebar-module sidebar-module-inset">
					<h4>Investimentos Modalidades</h4>
					<p>Nesta página você encontrará informações do governo
						relativas as Olímpiadas de 2016</p>
				</div>
				<!-- coluna modalidades -->
				<div class="sidebar-module">
					<h4>Modalidades</h4>
					<ol class="list-unstyled">
						<%
							for (int j = 0; j < nomes.size(); j++) {
						%>
						<li><a href="modalidades?valor=<%=nomes.get(j)%>"><%=nomes.get(j)%></a>
						</li>
						<%
							}
						%>
					</ol>

				</div>
				<!-- /coluna modalidades -->
				<!-- boloco links externos -->
				<div class="sidebar-module">
					<h4>Outros</h4>
					<ol class="list-unstyled">
						<li><a
							href="http://www.portaldatransparencia.gov.br/rio2016/">Portal
								da Transparencia</a></li>
					</ol>
				</div>
				<!-- /boloco links externos -->
			</div>

		</div>

	</div>

	<footer class="blog-footer">
		<p>
			Olímpiadas Rio 2016 <a href="http://getbootstrap.com">Bootstrap</a>
			by NITEIP.
		</p>
		<p>
			<a href="#">Back to top</a>
		</p>
	</footer>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script
		src="C:\Users\Matheus\Dropbox\5° Semestre\PIV - Dev Project\dist/js/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="C:\Users\Matheus\Dropbox\5° Semestre\PIV - Dev Project\dist/assets/js/ie10-viewport-bug-workaround.js"></script>

	<script src="offcanvas.js"></script>
</body>
</html>
